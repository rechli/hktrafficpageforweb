﻿<%--<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="HKTrafficPage.aspx.cs" Inherits="HKTrafficPageForWeb.HKTrafficPage" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title></title>
</head>
<body>--%>

<%@ Page Title="Manage Account" Language="C#" MasterPageFile="~/Site.Master" AutoEventWireup="true" CodeBehind="HKTrafficPage.aspx.cs" Inherits="HKTrafficPageForWeb.HKTrafficPage" %>

<asp:Content ContentPlaceHolderID="MainContent" runat="server">

    <%--    <form id="form1" runat="server">--%>
    <br />
    <br />
    <div id="divForData" style="font-family: 'Microsoft YaHei'; font-size: 14px;">
        <asp:GridView ID="GV_ForCountNumber" runat="server" CellPadding="4" ForeColor="#333333" GridLines="None" AutoGenerateColumns="False">
            <AlternatingRowStyle BackColor="White" />
            <Columns>
                <asp:BoundField DataField="SubTaskType" HeaderText="Sub TaskType">
                    <ItemStyle Width="16%" />
                </asp:BoundField>
                <asp:BoundField DataField="HKPages" HeaderText="HK Pages">
                    <ItemStyle Width="14%" />
                </asp:BoundField>
                <asp:BoundField DataField="MNLPages" HeaderText="MNL Pages">
                    <ItemStyle Width="14%" />
                </asp:BoundField>
                <asp:BoundField DataField="SZPages" HeaderText="SZ Pages">
                    <ItemStyle Width="14%" />
                </asp:BoundField>
                <asp:BoundField DataField="BJPages" HeaderText="BJ Pages">
                    <ItemStyle Width="14%" />
                </asp:BoundField>
                <asp:BoundField DataField="TWPages" HeaderText="TW Pages">
                    <HeaderStyle Width="14px" />
                    <ItemStyle Width="14%" />
                </asp:BoundField>
                <asp:BoundField DataField="SGPages" HeaderText="SG Pages">
                    <ItemStyle Width="14%" />
                </asp:BoundField>
            </Columns>
            <EditRowStyle BackColor="#2461BF" />
            <FooterStyle BackColor="#507CD1" Font-Bold="True" ForeColor="White" />
            <HeaderStyle BackColor="#507CD1" Font-Bold="True" ForeColor="White" />
            <PagerStyle BackColor="#2461BF" ForeColor="White" HorizontalAlign="Center" />
            <RowStyle BackColor="#EFF3FB" />
            <SelectedRowStyle BackColor="#D1DDF1" Font-Bold="True" ForeColor="#333333" />
            <SortedAscendingCellStyle BackColor="#F5F7FB" />
            <SortedAscendingHeaderStyle BackColor="#6D95E1" />
            <SortedDescendingCellStyle BackColor="#E9EBEF" />
            <SortedDescendingHeaderStyle BackColor="#4870BE" />
        </asp:GridView>
<%--        <br />
        <br />
        <div><table><tr>
                        <td>Job ID:</td><td><asp:TextBox ID="TextBoxJobID" runat="server" Width="200px"></asp:TextBox></td><td>&nbsp;&nbsp;&nbsp;&nbsp;</td>
                        <td>Job Name:</td><td><asp:TextBox ID="TextBoxJobName" runat="server" Width="200px"></asp:TextBox></td><td>&nbsp;&nbsp;&nbsp;&nbsp;</td>
                        <td><asp:Button ID="ButtonSearch" runat="server" OnClick="ButtonSearch_Click" Text="Search" /></td> 
                    </tr>
             </table>
        </div>--%>
        <asp:GridView ID="GridView1" runat="server" CellPadding="4" ForeColor="#333333" GridLines="None" AutoGenerateColumns="False" OnDataBound="GridView1_DataBound" AllowSorting="True" OnSorting="GridView1_Sorting" Width="98%">
            <AlternatingRowStyle BackColor="White" />
            <Columns>
                <asp:TemplateField>
                    <ItemTemplate>
                        <asp:Label ID="Label_TaskIn" runat="server" Visible="false" Text='<%# Eval("TaskIndicator") %>'></asp:Label>
                        <asp:Label ID="Label_TaskRevsion" runat="server" Visible="false" Text='<%# Eval("IsRevision") %>'></asp:Label>
                        <asp:Image ID="Image_R" runat="server" ImageUrl="~/Images/Revision.bmp" Visible="false" />
                        <asp:Image ID="Image_U" runat="server" ImageUrl="~/Images/Update.bmp" Visible="false" />
                        <asp:Image ID="Image_L" runat="server" ImageUrl="~/Images/LateRider.bmp" Visible="false" />
                        <asp:Image ID="Image_UL" runat="server" ImageUrl="~/Images/UandL.bmp" Visible="false" />

                    </ItemTemplate>
                </asp:TemplateField>
                <asp:BoundField DataField="TaskId" HeaderText="Task Id" SortExpression="TaskId" >
                <ItemStyle Width="10%" />
                </asp:BoundField>
                <asp:BoundField DataField="JobName" HeaderText="Job Name" SortExpression="JobName">
                    <ItemStyle Width="15%" />
                </asp:BoundField>
                <asp:BoundField DataField="TaskLanguage" HeaderText="Lang" />
                <asp:BoundField DataField="SubTaskType" HeaderText="Sub Task Type"></asp:BoundField>
                <asp:BoundField DataField="TeamName" HeaderText="Assigned Team" SortExpression="TeamName"></asp:BoundField>
                <asp:BoundField DataField="TotalNoOfPages" HeaderText="Pages" SortExpression="TotalNoOfPages" />
                <asp:BoundField DataField="TaT" HeaderText="Tat" />
                <asp:BoundField DataField="CreationDate" HeaderText="Created Time">
                    <ItemStyle Width="15%" />
                </asp:BoundField>
            </Columns>
            <EditRowStyle BackColor="#2461BF" />
            <FooterStyle BackColor="#507CD1" Font-Bold="True" ForeColor="White" />
            <HeaderStyle BackColor="#337ab7" Font-Bold="True" ForeColor="White" HorizontalAlign="Left" />
            <PagerStyle BackColor="#2461BF" ForeColor="White" HorizontalAlign="Center" />
            <RowStyle BackColor="#EFF3FB" />
            <SelectedRowStyle BackColor="#D1DDF1" Font-Bold="True" ForeColor="#333333" />
            <SortedAscendingCellStyle BackColor="#F5F7FB" />
            <SortedAscendingHeaderStyle BackColor="#6D95E1" />
            <SortedDescendingCellStyle BackColor="#E9EBEF" />
            <SortedDescendingHeaderStyle BackColor="#4870BE" />
        </asp:GridView>
    </div>

    <script type="text/javascript">
        var currentDiv = this.document.getElementById("MainContent_GridView1");
        this.document.getElementById("MainContent_GV_ForCountNumber").style.width = window.getComputedStyle(currentDiv).width;

        window.onresize = function () {
            var currentDiv = this.document.getElementById("MainContent_GridView1");
            this.document.getElementById("MainContent_GV_ForCountNumber").style.width = window.getComputedStyle(currentDiv).width;
        };
    </script>
    <%--    </form>--%>
</asp:Content>
<%--</body>
</html>--%>
