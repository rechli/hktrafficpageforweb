﻿using System.Collections.Generic;
using System.Runtime.Serialization;
using HKTrafficPageForWeb;
namespace HKTrafficPageForWeb
{
    [DataContract]
    public class ResponseWithObjectDTO<T>
    {
        //Generic Data
        [DataMember]
        public T data { get; set; }
        [DataMember]
        public int ErrorCode { get; set; }
        [DataMember]
        public string Message { get; set; }
        [DataMember]
        public int ResultType { get; set; }
        [DataMember]
        public object StackTrace { get; set; }
        [DataMember]
        public object ValidationError { get; set; }
        [DataMember]
        public int FailureType { get; set; }
    }
}