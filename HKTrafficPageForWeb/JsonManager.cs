﻿using System.Collections.Generic;
using System.Linq;
using Newtonsoft.Json.Linq;
using HKTrafficPageForWeb;
namespace HKTrafficPageForWeb
{
    public class JsonManager
    {
        public ResponseDTO<T> ParseJsonString<T>(string result)
        {
            var token = JToken.Parse(result);

            var child = token.FindTokens("Data");

            //if the data contain array, use ilist<T> to read the json.
            if (child.Count > 0 && child.First() is JArray)
            {

                return token.ToObject<ResponseDTO<T>>();
            }
            else //if the data contain no array, use <T> to read the json.
            {

                var returnResult = token.ToObject<ResponseWithObjectDTO<T>>();
                //return responseDTO only to make the result consistence.
                return ToResponseDTO<T>(returnResult);
            }
        }

        private ResponseDTO<T> ToResponseDTO<T>(ResponseWithObjectDTO<T> result)
        {
            ResponseDTO<T> t = new ResponseDTO<T>();
            t.Data = new List<T>();
            t.Data.Add(result.data);
            t.Message = result.Message;
            t.ResultType = result.ResultType;
            t.StackTrace = result.StackTrace;
            t.FailureType = result.FailureType;
            t.ErrorCode = result.ErrorCode;
            t.ValidationError = result.ValidationError;
            return t;
        }
    }
}