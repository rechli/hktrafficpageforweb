﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace HKTrafficPageForWeb.Account
{
    public partial class Lockout : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            //FormsAuthentication.SignOut();
            Response.SetCookie(new HttpCookie("userName", string.Empty));            
            IdentityHelper.RedirectToReturnUrl("~/Account/Login.aspx", Response);
        }
    }
}