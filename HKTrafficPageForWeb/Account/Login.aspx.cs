﻿using System;
using System.Web;
using System.Web.UI;
using Microsoft.AspNet.Identity;
using Microsoft.AspNet.Identity.Owin;
using Owin;
using HKTrafficPageForWeb.Models;
using System.Web.Security;

namespace HKTrafficPageForWeb.Account
{
    public partial class Login : Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            
            //RegisterHyperLink.NavigateUrl = "Register";
            //// Enable this once you have account confirmation enabled for password reset functionality
            ////ForgotPasswordHyperLink.NavigateUrl = "Forgot";
            //OpenAuthLogin.ReturnUrl = Request.QueryString["ReturnUrl"];
            //var returnUrl = HttpUtility.UrlEncode(Request.QueryString["ReturnUrl"]);
            //if (!String.IsNullOrEmpty(returnUrl))
            //{
            //    RegisterHyperLink.NavigateUrl += "?ReturnUrl=" + returnUrl;
            //}
        }

        
        protected void LogIn(object sender, EventArgs e)
        {
            if (IsValid)
            {
                RestClient.init();
                TCCLoginDTO tccLoginDTO = RestClient.Login(Email.Text, Password.Text);

                
                switch(tccLoginDTO.StatusCode)
                {
                    case System.Net.HttpStatusCode.OK:
                        //IdentityHelper.RedirectToReturnUrl(Request.QueryString["ReturnUrl"], Response);
                        //FormsAuthentication.SetAuthCookie(Email.Text, true, tccLoginDTO.Token);
                        FormsAuthentication.SetAuthCookie(Email.Text, true);
                        Response.SetCookie(new HttpCookie("userName",Email.Text));
                        IdentityHelper.RedirectToReturnUrl("~/HKTrafficPage.aspx", Response);
                        break;
                    //case System.Net.HttpStatusCode.:
                    //    Response.Redirect("/Account/Lockout");
                    //    break;
                    //case SignInStatus.RequiresVerification:
                    //    Response.Redirect(String.Format("/Account/TwoFactorAuthenticationSignIn?ReturnUrl={0}&RememberMe={1}",
                    //                                    Request.QueryString["ReturnUrl"],
                    //                                    RememberMe.Checked),
                    //                      true);
                    //    break;
                    //case System.Net.HttpStatusCode.Unauthorized:
                        //Response.Redirect("/Account/Lockout");
                        //break;

                    
                    default:
                        FailureText.Text = "Invalid login attempt";
                        ErrorMessage.Visible = true;
                        break;
                }
            }

                //if (IsValid)
                //{
                //    // Validate the user password
                //    //var manager = Context.GetOwinContext().GetUserManager<ApplicationUserManager>();
                //    //var signinManager = Context.GetOwinContext().GetUserManager<ApplicationSignInManager>();

                //    // This doen't count login failures towards account lockout
                //    // To enable password failures to trigger lockout, change to shouldLockout: true
                //    //var result = signinManager.PasswordSignIn(Email.Text, Password.Text, RememberMe.Checked, shouldLockout: false);               

                //    //switch (result)
                //    //{
                //    //    case SignInStatus.Success:
                //    //        IdentityHelper.RedirectToReturnUrl(Request.QueryString["ReturnUrl"], Response);
                //    //        break;
                //    //    case SignInStatus.LockedOut:
                //    //        Response.Redirect("/Account/Lockout");
                //    //        break;
                //    //    case SignInStatus.RequiresVerification:
                //    //        Response.Redirect(String.Format("/Account/TwoFactorAuthenticationSignIn?ReturnUrl={0}&RememberMe={1}", 
                //    //                                        Request.QueryString["ReturnUrl"],
                //    //                                        RememberMe.Checked),
                //    //                          true);
                //    //        break;
                //    //    case SignInStatus.Failure:
                //    //    default:
                //    //        FailureText.Text = "Invalid login attempt";
                //    //        ErrorMessage.Visible = true;
                //    //        break;
                //    //}


                //}
            }

        
    }
}