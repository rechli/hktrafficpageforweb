﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.IO;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Text;
using System.Web.Http;
using System.Xml;

namespace HKTrafficPageForWeb
{
    public class WebApiController : ApiController
    {
        [HttpGet]
        public string hundan()
        {

            return "11 hundan";
        }
        [HttpGet]
        public string wanbbadan()
        {
            return "11 wanbbadan";
        }

        // GET api/wechat
        [HttpGet]
        public HttpResponseMessage GetSZPMTrafficData()
        {

            string result = GetData("", "", true);

            var response = Request.CreateResponse(HttpStatusCode.OK);

            response.Content = new StringContent(result, Encoding.UTF8);
            response.Headers.CacheControl = new CacheControlHeaderValue()
            {
                MaxAge = TimeSpan.FromMinutes(20)
            };

            return response;
        }

        [HttpGet]
        public HttpResponseMessage GetSZQCData(string argJobID, string argJobName, bool isPmBucket)
        {

            string result = GetDataForSZQC(argJobID, argJobName, isPmBucket);

            var response = Request.CreateResponse(HttpStatusCode.OK);

            response.Content = new StringContent(result, Encoding.UTF8);
            response.Headers.CacheControl = new CacheControlHeaderValue()
            {
                MaxAge = TimeSpan.FromMinutes(20)
            };

            return response;
        }

        [HttpGet]
        public HttpResponseMessage GetSZTrafficData(string argJobID, string argJobName, bool isPmBucket)
        {

            string result = GetDataForSZTraffic(argJobID, argJobName, isPmBucket);

            var response = Request.CreateResponse(HttpStatusCode.OK);

            response.Content = new StringContent(result, Encoding.UTF8);
            response.Headers.CacheControl = new CacheControlHeaderValue()
            {
                MaxAge = TimeSpan.FromMinutes(20)
            };

            return response;
        }

        [HttpGet]
        public HttpResponseMessage GetTicketNoTask(string ticketNo)
        {
            string taskIdentity = "";
            string sqlText = "select top 1 TaskIdentity from Tasks where LatestTicketNumber=" + ticketNo + " order by CreationDate desc";
            DataTable dt = SqlHelper.ExecuteDataTable(SqlHelper.ConnectionString, CommandType.Text, sqlText);
            if (dt != null && dt.Rows.Count > 0)
            {
                taskIdentity = dt.Rows[0][0].ToString();

            }
            var response = Request.CreateResponse(HttpStatusCode.OK);

            response.Content = new StringContent(taskIdentity, Encoding.UTF8);
            response.Headers.CacheControl = new CacheControlHeaderValue()
            {
                MaxAge = TimeSpan.FromMinutes(20)
            };

            return response;
        }

        [HttpGet]
        public HttpResponseMessage GetFileLocationTxtByTaskId(string taskId)
        {
            string result = "";

            string sqlText = "select top 2 t1.TeamName as engLocation,t2.TeamName as chiLocation from JobFileLocationMapping jfm "
                               + "left join Teams t1 on jfm.EngFileLocationId=t1.TeamId "
                               + "left join Teams t2 on jfm.ChiFileLocationId=t2.TeamId "
                               + "inner join Jobs j on jfm.JobIdentity=j.JobIdentity "
                               + "left join Tasks t on t.JobIdentity=j.JobIdentity "
                               + "where (t.taskId='" + taskId + "') and (jfm.teamId =39 or jfm.TeamId is null) order by jfm.TeamId desc";
            DataTable dt = SqlHelper.ExecuteDataTable(SqlHelper.ConnectionString, CommandType.Text, sqlText);
            if (dt != null && dt.Rows.Count > 0 && dt.Rows.Count == 1)
            {
                string engLocaion = dt.Rows[0][0].ToString();
                string chiLocaion = dt.Rows[0][1].ToString();
                string tt = taskId + "    " + "Eng: " + engLocaion + "   Chi:" + chiLocaion;
                result = tt;
            }
            else if (dt != null && dt.Rows.Count > 0 && dt.Rows.Count == 2)
            {
                string engLocaion = dt.Rows[0][0].ToString();
                string chiLocaion = dt.Rows[0][1].ToString();
                string tt = taskId + "  SZ PM:  " + "Eng: " + engLocaion + "   Chi:" + chiLocaion;

                string engLocaion2 = dt.Rows[1][0].ToString();
                string chiLocaion2 = dt.Rows[1][1].ToString();
                tt += "\r\n" + taskId + "  Default:  " + "Eng: " + engLocaion2 + "   Chi:" + chiLocaion2;
                result = tt;
            }
            else
            {
                result = taskId + "    " + "Can not find the result.";
            }

            var response = Request.CreateResponse(HttpStatusCode.OK);

            response.Content = new StringContent(result, Encoding.UTF8);
            response.Headers.CacheControl = new CacheControlHeaderValue()
            {
                MaxAge = TimeSpan.FromMinutes(20)
            };

            return response;
        }

        [HttpGet]
        public HttpResponseMessage GetFileLocationTxtByJobId(string jobId)
        {
            string result = "";

            string sqlText = "select top 2 t1.TeamName as engLocation,t2.TeamName as chiLocation from JobFileLocationMapping jfm "
                            + "left join Teams t1 on jfm.EngFileLocationId=t1.TeamId "
                            + "left join Teams t2 on jfm.ChiFileLocationId=t2.TeamId "
                            + "inner join Jobs j on jfm.JobIdentity=j.JobIdentity "
                            + "where (j.JobId='" + jobId + "') and (jfm.teamId =39 or jfm.TeamId is null) order by jfm.TeamId desc";
            DataTable dt = SqlHelper.ExecuteDataTable(SqlHelper.ConnectionString, CommandType.Text, sqlText);
            if (dt != null && dt.Rows.Count > 0 && dt.Rows.Count == 1)
            {
                string engLocaion = dt.Rows[0][0].ToString();
                string chiLocaion = dt.Rows[0][1].ToString();
                string tt = jobId + "    " + "Eng: " + engLocaion + "   Chi:" + chiLocaion;
                result = tt;
            }
            else if (dt != null && dt.Rows.Count > 0 && dt.Rows.Count == 2)
            {
                string engLocaion = dt.Rows[0][0].ToString();
                string chiLocaion = dt.Rows[0][1].ToString();
                string tt = jobId + "  SZ PM:  " + "Eng: " + engLocaion + "   Chi:" + chiLocaion;

                string engLocaion2 = dt.Rows[1][0].ToString();
                string chiLocaion2 = dt.Rows[1][1].ToString();
                tt += "\r\n" + jobId + "  Default:  " + "Eng: " + engLocaion2 + "   Chi:" + chiLocaion2;
                result = tt;
            }
            else
            {
                result = jobId + "    " + "Can not find the result.";
            }

            var response = Request.CreateResponse(HttpStatusCode.OK);

            response.Content = new StringContent(result, Encoding.UTF8);
            response.Headers.CacheControl = new CacheControlHeaderValue()
            {
                MaxAge = TimeSpan.FromMinutes(20)
            };

            return response;
        }

        public string GetData(string argJobID, string argJobName, bool isPmBucket)
        {

            DateTime fromTime = DateTime.Now.AddDays(-7);
            DateTime toTime = DateTime.Now.AddDays(1);
            string storePro = "GetOutStandingTaskInfoForSZPM";

            SqlParameter[] sqlParameters = new SqlParameter[6];

            if (argJobName != null)
            { sqlParameters[0] = new SqlParameter("@jobName", argJobName); }
            else
            { sqlParameters[0] = new SqlParameter("@jobName", string.Empty); }

            sqlParameters[1] = new SqlParameter("@isShowPm", isPmBucket);
            sqlParameters[2] = new SqlParameter("@dateFrom", fromTime);
            sqlParameters[3] = new SqlParameter("@dateTo", toTime);

            if (argJobID != null)
            { sqlParameters[4] = new SqlParameter("@jobId", argJobID); }
            else
            { sqlParameters[4] = new SqlParameter("@jobId", string.Empty); }

            sqlParameters[5] = new SqlParameter("@isDoneBuctet", 0);

            DataSet ds = SqlHelper.ExecuteDataset(SqlHelper.ConnectionString, CommandType.StoredProcedure, storePro, sqlParameters);
            if (ds.Tables.Count > 0)
            {
                return GenTxt(ds);
            }
            else
                return "";
        }

        public string GetDataForSZQC(string argJobID, string argJobName, bool isPmBucket)
        {

            DateTime fromTime = DateTime.Now.AddDays(-7);
            DateTime toTime = DateTime.Now.AddDays(1);
            string storePro = "GetOutStandingTaskInfoForSZPM";

            SqlParameter[] sqlParameters = new SqlParameter[6];

            if (argJobName != null)
            { sqlParameters[0] = new SqlParameter("@jobName", argJobName); }
            else
            { sqlParameters[0] = new SqlParameter("@jobName", string.Empty); }

            sqlParameters[1] = new SqlParameter("@isShowPm", isPmBucket);
            sqlParameters[2] = new SqlParameter("@dateFrom", fromTime);
            sqlParameters[3] = new SqlParameter("@dateTo", toTime);

            if (argJobID != null)
            { sqlParameters[4] = new SqlParameter("@jobId", argJobID); }
            else
            { sqlParameters[4] = new SqlParameter("@jobId", string.Empty); }

            sqlParameters[5] = new SqlParameter("@isDoneBuctet", 0);

            DataSet ds = SqlHelper.ExecuteDataset(SqlHelper.ConnectionString, CommandType.StoredProcedure, storePro, sqlParameters);
            if (ds.Tables.Count > 0)
            {
                return GenTxt(ds);
            }
            else
                return "";
        }

        [HttpGet]
        public HttpResponseMessage GetTaskAttachmentInfo(string argTaskIdentity)
        {
            string result = string.Empty;

            //DateTime fromTime = DateTime.Now.AddDays(-7);
            //DateTime toTime = DateTime.Now.AddDays(1);
            string storePro = "GetTaskAttachmentInfo";

            SqlParameter[] sqlParameters = new SqlParameter[1];
            sqlParameters[0] = new SqlParameter("@taskIdentity", argTaskIdentity);

            DataSet ds = SqlHelper.ExecuteDataset(SqlHelper.ConnectionString, CommandType.StoredProcedure, storePro, sqlParameters);
            if (ds.Tables.Count > 0)
            {
                result = GenTxt(ds);
            }

            var response = Request.CreateResponse(HttpStatusCode.OK);

            response.Content = new StringContent(result, Encoding.UTF8);
            response.Headers.CacheControl = new CacheControlHeaderValue()
            {
                MaxAge = TimeSpan.FromMinutes(20)
            };

            return response;
        }


        [HttpGet]
        public HttpResponseMessage InsertPrintingRecords(string argTaskID, int argTicketNo, string argStampNo, string argHostName, string argIP)
        {
            string connectionStr = System.Configuration.ConfigurationManager.AppSettings["DbServiceForQC"].ToString();
            //string result = string.Empty;

            //DateTime fromTime = DateTime.Now.AddDays(-7);
            //DateTime toTime = DateTime.Now.AddDays(1);
            string storePro = "InsertPrintingRecords";

            SqlParameter[] sqlParameters = new SqlParameter[5];

            sqlParameters[0] = new SqlParameter("@TaskID", argTaskID);
            sqlParameters[1] = new SqlParameter("@TicketNo", argTicketNo);
            sqlParameters[2] = new SqlParameter("@StampNo", argStampNo);
            sqlParameters[3] = new SqlParameter("@HostName", argHostName);
            sqlParameters[4] = new SqlParameter("@IP", argIP);

            int result = SqlHelper.ExecuteNonQuery(connectionStr, CommandType.StoredProcedure, storePro, sqlParameters);
            var response = Request.CreateResponse(HttpStatusCode.OK);

            response.Content = new StringContent(result.ToString(), Encoding.UTF8);
            response.Headers.CacheControl = new CacheControlHeaderValue()
            {
                MaxAge = TimeSpan.FromMinutes(10)
            };

            return response;
        }

        [HttpGet]
        public HttpResponseMessage UpdateWorkingStatus(string argPanelID = "", string argMorning = "", string argMorningL = " ", string argNoon = "", string argNoonL = " ", string argNight = "", string argNightL = " ", string argDTPIP = "", string argVDI = "", string argPosition = "", string argStatus = "", int argLocationID = 0, string argTime = "")
        {
            string connectionStr = System.Configuration.ConfigurationManager.AppSettings["DbServiceForQC"].ToString();
            //string result = string.Empty;

            //DateTime fromTime = DateTime.Now.AddDays(-7);
            //DateTime toTime = DateTime.Now.AddDays(1);
            string storePro = "UpdateWorkingStatus";

            SqlParameter[] sqlParameters = new SqlParameter[13];


            sqlParameters[0] = new SqlParameter("@PanelID", argPanelID);
            sqlParameters[1] = new SqlParameter("@Morning", argMorning);
            sqlParameters[2] = new SqlParameter("@MorningLevel", argMorningL);
            sqlParameters[3] = new SqlParameter("@Noon", argNoon);
            sqlParameters[4] = new SqlParameter("@NoonLevel", argNoonL);
            sqlParameters[5] = new SqlParameter("@Night", argNight);
            sqlParameters[6] = new SqlParameter("@NightLevel", argNightL);
            sqlParameters[7] = new SqlParameter("@DTPIP", argDTPIP);
            sqlParameters[8] = new SqlParameter("@VDI", argVDI);
            sqlParameters[9] = new SqlParameter("@Position", argPosition);
            sqlParameters[10] = new SqlParameter("@Status", argStatus);
            sqlParameters[11] = new SqlParameter("@LocationID", argLocationID);
            sqlParameters[12] = new SqlParameter("@Time", " ");

            //sqlParameters[10] = new SqlParameter("@ID", argID);

            int result = SqlHelper.ExecuteNonQuery(connectionStr, CommandType.StoredProcedure, storePro, sqlParameters);
            var response = Request.CreateResponse(HttpStatusCode.OK);

            response.Content = new StringContent(result.ToString(), Encoding.UTF8);
            response.Headers.CacheControl = new CacheControlHeaderValue()
            {
                MaxAge = TimeSpan.FromMinutes(10)
            };

            return response;
        }


        [HttpGet]
        public HttpResponseMessage UpdateWorkingCurrentShiftStatus(string argPanelID, string argCurrentShift)
        {
            string connectionStr = System.Configuration.ConfigurationManager.AppSettings["DbServiceForQC"].ToString();
            //string result = string.Empty;

            //DateTime fromTime = DateTime.Now.AddDays(-7);
            //DateTime toTime = DateTime.Now.AddDays(1);
            string storePro = "UpdateWorkingCurrentShiftStatus";

            SqlParameter[] sqlParameters = new SqlParameter[2];


            sqlParameters[0] = new SqlParameter("@PanelID", argPanelID);
            sqlParameters[1] = new SqlParameter("@CurrentShift", argCurrentShift);

            //sqlParameters[10] = new SqlParameter("@ID", argID);

            int result = SqlHelper.ExecuteNonQuery(connectionStr, CommandType.StoredProcedure, storePro, sqlParameters);
            var response = Request.CreateResponse(HttpStatusCode.OK);

            response.Content = new StringContent(result.ToString(), Encoding.UTF8);
            response.Headers.CacheControl = new CacheControlHeaderValue()
            {
                MaxAge = TimeSpan.FromMinutes(10)
            };

            return response;
        }

        [HttpGet]
        public HttpResponseMessage AddWorkingStatus(string argPanelID = "", string argMorning = "", string argMorningL = " ", string argNoon = "", string argNoonL = " ", string argNight = "", string argNightL = " ", string argDTPIP = "", string argVDI = "", string argPosition = "", string argStatus = "", string argLocationID = "", string argTime = "")
        {
            string connectionStr = System.Configuration.ConfigurationManager.AppSettings["DbServiceForQC"].ToString();
            //string result = string.Empty;
            //DateTime fromTime = DateTime.Now.AddDays(-7);
            //DateTime toTime = DateTime.Now.AddDays(1);
            string storePro = "AddWorkingStatus";

            SqlParameter[] sqlParameters = new SqlParameter[14];

            sqlParameters[0] = new SqlParameter("@PanelID", argPanelID);
            sqlParameters[1] = new SqlParameter("@Morning", argMorning);
            sqlParameters[2] = new SqlParameter("@MorningLevel", argMorningL);
            sqlParameters[3] = new SqlParameter("@Noon", argNoon);
            sqlParameters[4] = new SqlParameter("@NoonLevel", argNoonL);
            sqlParameters[5] = new SqlParameter("@Night", argNight);
            sqlParameters[6] = new SqlParameter("@NightLevel", argNightL);
            sqlParameters[7] = new SqlParameter("@DTPIP", argDTPIP);
            sqlParameters[8] = new SqlParameter("@VDI", argVDI);
            sqlParameters[9] = new SqlParameter("@Position", argPosition);
            sqlParameters[10] = new SqlParameter("@Status", argStatus);
            sqlParameters[11] = new SqlParameter("@Time", " ");
            sqlParameters[12] = new SqlParameter("@CurrentShift", " ");
            sqlParameters[13] = new SqlParameter("@LocationID", argLocationID);

            int result = SqlHelper.ExecuteNonQuery(connectionStr, CommandType.StoredProcedure, storePro, sqlParameters);
            var response = Request.CreateResponse(HttpStatusCode.OK);

            response.Content = new StringContent(result.ToString(), Encoding.UTF8);
            response.Headers.CacheControl = new CacheControlHeaderValue()
            {
                MaxAge = TimeSpan.FromMinutes(10)
            };

            return response;
        }

        [HttpGet]
        public HttpResponseMessage AddWorkingStatusForQC(string argPanelID = "", string argMorning = "", string argMorningL = " ", string argNoon = "", string argNoonL = " ", string argNight = "", string argNightL = " ", string argDTPIP = "", string argVDI = "", string argPosition = "", string argStatus = "", string argLocationID = "", string argTime = "", string argDepartment = "")
        {
            string connectionStr = System.Configuration.ConfigurationManager.AppSettings["DbServiceForQC"].ToString();
            //string result = string.Empty;
            //DateTime fromTime = DateTime.Now.AddDays(-7);
            //DateTime toTime = DateTime.Now.AddDays(1);
            string storePro = "AddWorkingStatusForQC";

            SqlParameter[] sqlParameters = new SqlParameter[15];

            sqlParameters[0] = new SqlParameter("@PanelID", argPanelID);
            sqlParameters[1] = new SqlParameter("@Morning", argMorning);
            sqlParameters[2] = new SqlParameter("@MorningLevel", argMorningL);
            sqlParameters[3] = new SqlParameter("@Noon", argNoon);
            sqlParameters[4] = new SqlParameter("@NoonLevel", argNoonL);
            sqlParameters[5] = new SqlParameter("@Night", argNight);
            sqlParameters[6] = new SqlParameter("@NightLevel", argNightL);
            sqlParameters[7] = new SqlParameter("@DTPIP", argDTPIP);
            sqlParameters[8] = new SqlParameter("@VDI", argVDI);
            sqlParameters[9] = new SqlParameter("@Position", argPosition);
            sqlParameters[10] = new SqlParameter("@Status", argStatus);
            sqlParameters[11] = new SqlParameter("@Time", " ");
            sqlParameters[12] = new SqlParameter("@CurrentShift", " ");
            sqlParameters[13] = new SqlParameter("@LocationID", argLocationID);
            sqlParameters[14] = new SqlParameter("@Department", argDepartment);

            int result = SqlHelper.ExecuteNonQuery(connectionStr, CommandType.StoredProcedure, storePro, sqlParameters);
            var response = Request.CreateResponse(HttpStatusCode.OK);

            response.Content = new StringContent(result.ToString(), Encoding.UTF8);
            response.Headers.CacheControl = new CacheControlHeaderValue()
            {
                MaxAge = TimeSpan.FromMinutes(10)
            };

            return response;
        }

        [HttpGet]
        public HttpResponseMessage DeleteWorkingStatus(string argPanelID)
        {
            string connectionStr = System.Configuration.ConfigurationManager.AppSettings["DbServiceForQC"].ToString();
            //string result = string.Empty;

            //DateTime fromTime = DateTime.Now.AddDays(-7);
            //DateTime toTime = DateTime.Now.AddDays(1);
            string storePro = "DeleteWorkingStatus";

            SqlParameter[] sqlParameters = new SqlParameter[1];

            sqlParameters[0] = new SqlParameter("@PanelID", argPanelID);


            int result = SqlHelper.ExecuteNonQuery(connectionStr, CommandType.StoredProcedure, storePro, sqlParameters);
            var response = Request.CreateResponse(HttpStatusCode.OK);

            response.Content = new StringContent(result.ToString(), Encoding.UTF8);
            response.Headers.CacheControl = new CacheControlHeaderValue()
            {
                MaxAge = TimeSpan.FromMinutes(10)
            };

            return response;
        }



        [HttpGet]
        public HttpResponseMessage GetYesterdayAndTodayPrintingRecords()
        {
            string connectionStr = System.Configuration.ConfigurationManager.AppSettings["DbServiceForQC"].ToString();
            string result = string.Empty;
            string storePro = "GetYesterdayAndTodayPrintingRecords";
            DataSet ds = SqlHelper.ExecuteDataset(connectionStr, CommandType.StoredProcedure, storePro);
            if (ds.Tables.Count > 0)
            {
                result = GenTxt(ds);
            }

            var response = Request.CreateResponse(HttpStatusCode.OK);

            response.Content = new StringContent(result, Encoding.UTF8);
            response.Headers.CacheControl = new CacheControlHeaderValue()
            {
                MaxAge = TimeSpan.FromMinutes(20)
            };

            return response;
        }



        [HttpGet]
        public HttpResponseMessage GetWorkingStatus(string argLocationID)
        {
            string connectionStr = System.Configuration.ConfigurationManager.AppSettings["DbServiceForQC"].ToString();
            string result = string.Empty;
            string storePro = "GetWorkingStatus";

            SqlParameter[] sqlParameters = new SqlParameter[1];
            sqlParameters[0] = new SqlParameter("@LocationID", argLocationID);

            DataSet ds = SqlHelper.ExecuteDataset(connectionStr, CommandType.StoredProcedure, storePro, sqlParameters);
            if (ds.Tables.Count > 0)
            {
                result = GenTxt(ds);
            }

            var response = Request.CreateResponse(HttpStatusCode.OK);

            response.Content = new StringContent(result, Encoding.UTF8);
            response.Headers.CacheControl = new CacheControlHeaderValue()
            {
                MaxAge = TimeSpan.FromMinutes(20)
            };

            return response;
        }

        [HttpGet]
        public HttpResponseMessage GetWorkingStatusForQC(string argLocationID, string argDepartment)
        {
            string connectionStr = System.Configuration.ConfigurationManager.AppSettings["DbServiceForQC"].ToString();
            string result = string.Empty;
            string storePro = "GetWorkingStatusForQC";

            SqlParameter[] sqlParameters = new SqlParameter[2];
            sqlParameters[0] = new SqlParameter("@LocationID", argLocationID);
            sqlParameters[1] = new SqlParameter("@Department", argDepartment);

            DataSet ds = SqlHelper.ExecuteDataset(connectionStr, CommandType.StoredProcedure, storePro, sqlParameters);
            if (ds.Tables.Count > 0)
            {
                result = GenTxt(ds);
            }

            var response = Request.CreateResponse(HttpStatusCode.OK);

            response.Content = new StringContent(result, Encoding.UTF8);
            response.Headers.CacheControl = new CacheControlHeaderValue()
            {
                MaxAge = TimeSpan.FromMinutes(20)
            };

            return response;
        }


        public string GetDataForSZTraffic(string argJobID, string argJobName, bool isPmBucket)
        {

            DateTime fromTime = DateTime.Now.AddDays(-7);
            DateTime toTime = DateTime.Now.AddDays(1);
            string storePro = "GetOutStandingTaskInfoForSZTraffic";

            SqlParameter[] sqlParameters = new SqlParameter[6];

            if (argJobName != null)
            { sqlParameters[0] = new SqlParameter("@jobName", argJobName); }
            else
            { sqlParameters[0] = new SqlParameter("@jobName", string.Empty); }

            sqlParameters[1] = new SqlParameter("@isShowPm", isPmBucket);
            sqlParameters[2] = new SqlParameter("@dateFrom", fromTime);
            sqlParameters[3] = new SqlParameter("@dateTo", toTime);

            if (argJobID != null)
            { sqlParameters[4] = new SqlParameter("@jobId", argJobID); }
            else
            { sqlParameters[4] = new SqlParameter("@jobId", string.Empty); }

            sqlParameters[5] = new SqlParameter("@isDoneBuctet", 0);

            DataSet ds = SqlHelper.ExecuteDataset(SqlHelper.ConnectionString, CommandType.StoredProcedure, storePro, sqlParameters);
            if (ds.Tables.Count > 0)
            {
                return GenTxt(ds);
            }
            else
                return "";
        }

        private string GenTxt(DataSet ds)
        {
            string xmlData = ConvertDataTableToXML(ds.Tables[0]);

            try
            {
                return xmlData;
            }
            catch (Exception ex)
            {
                return "";
            }
        }

        private string ConvertDataTableToXML(DataTable xmlDS)
        {
            MemoryStream stream = null;
            XmlTextWriter writer = null;
            try
            {
                stream = new MemoryStream();
                writer = new XmlTextWriter(stream, Encoding.UTF8);
                xmlDS.WriteXml(writer);
                int count = (int)stream.Length;
                byte[] arr = new byte[count];
                stream.Seek(0, SeekOrigin.Begin);
                stream.Read(arr, 0, count);
                UTF8Encoding utf = new UTF8Encoding();
                return utf.GetString(arr).Trim();
            }
            catch
            {
                return String.Empty;
            }
            finally
            {
                if (writer != null) writer.Close();
            }
        }



        [HttpGet]
        public HttpResponseMessage GetTaskTAT()
        {
            DateTime fromDate = DateTime.Now.AddDays(-10);
            DateTime toDate = DateTime.Now.AddDays(1);
            string result = "";

            string sqlText = "select taskid, CONVERT(varchar(100), TAT, 20) as TAT from [dbo].[TaskTAT] t where t.ModifiedDate between  '" + fromDate + "'  and  '" + toDate + "'  order by t.TaskID desc";
            DataTable dt = SqlHelper.ExecuteDataTable(SqlHelper.QCConnectionString, CommandType.Text, sqlText);
            if (dt != null && dt.Rows.Count > 0)
            {
                string tt = "";
                for (int i = 0; i < dt.Rows.Count; i++)
                {
                    string TaskID = dt.Rows[i][0].ToString();
                    string TATstr = dt.Rows[i][1].ToString();
                    tt = tt + TaskID + "&" + TATstr + "|";
                }
                if (tt != "")
                    tt = tt.Substring(0, tt.Length - 1);
                result = tt;


                var response = Request.CreateResponse(HttpStatusCode.OK);

                response.Content = new StringContent(result, Encoding.UTF8);
                response.Headers.CacheControl = new CacheControlHeaderValue()
                {
                    MaxAge = TimeSpan.FromMinutes(20)
                };

                return response;
            }
            else
            {
                var response = Request.CreateResponse(HttpStatusCode.NoContent);
                return response;
            }
        }

        [HttpGet]
        public HttpResponseMessage GetTaskTATByTaskID(string argTaskId)
        {
            string sqlText = "select * from [dbo].[TaskTAT] t where  t.TaskID = " + argTaskId;
            DataTable dt = SqlHelper.ExecuteDataTable(SqlHelper.QCConnectionString, CommandType.Text, sqlText);
            if (dt != null && dt.Rows.Count > 0)
            {
                string TATstr = dt.Rows[0][1].ToString();

                string result = TATstr;
                var response = Request.CreateResponse(HttpStatusCode.OK);

                response.Content = new StringContent(result, Encoding.UTF8);
                response.Headers.CacheControl = new CacheControlHeaderValue()
                {
                    MaxAge = TimeSpan.FromMinutes(20)
                };

                return response;
            }
            else
            {
                var response = Request.CreateResponse(HttpStatusCode.NoContent);
                return response;
            }
        }

        [HttpGet]
        public HttpResponseMessage ChangeTaskTAT(string argTaskId, DateTime argTat)
        {
            string result = "";

            DataTable dt = ChangesTaskTAT(argTaskId, argTat);

            if (dt.Rows.Count > 0)
            {
                result = "Modified successfully!";
                var response = Request.CreateResponse(HttpStatusCode.OK);

                response.Content = new StringContent(result, Encoding.UTF8);
                response.Headers.CacheControl = new CacheControlHeaderValue()
                {
                    MaxAge = TimeSpan.FromMinutes(20)
                };

                return response;
            }
            else
            {
                var response = Request.CreateResponse(HttpStatusCode.NoContent);
                return response;
            }


        }

        [HttpGet]
        public HttpResponseMessage DeleteTaskTATByTaskId(string argTaskId)
        {
            try
            {

                string sqlText = "DELETE [dbo].[TaskTAT] where [TaskID]='" + argTaskId + "'";
                object obj = SqlHelper.ExecuteScalar(SqlHelper.QCConnectionString, CommandType.Text, sqlText);

                if (obj == null)
                {
                    string result = "Modified successfully!";
                    var response = Request.CreateResponse(HttpStatusCode.OK);

                    response.Content = new StringContent(result, Encoding.UTF8);
                    response.Headers.CacheControl = new CacheControlHeaderValue()
                    {
                        MaxAge = TimeSpan.FromMinutes(20)
                    };

                    return response;
                }
                else
                {
                    var response = Request.CreateResponse(HttpStatusCode.NoContent);
                    return response;
                }

            }
            catch (Exception ex)
            {
                var response = Request.CreateResponse(HttpStatusCode.NotModified);
                return response;
            }

        }



        public DataTable ChangesTaskTAT(string argTaskId, DateTime argTat)
        {

            DateTime fromTime = DateTime.Now.AddDays(-10);
            DateTime toTime = DateTime.Now.AddDays(1);
            string storePro = "ChangeTaskTATByTaskId";

            SqlParameter[] sqlParameters = new SqlParameter[5];

            sqlParameters[0] = new SqlParameter("@taskId", argTaskId);
            sqlParameters[1] = new SqlParameter("@taskTAT", argTat);
            sqlParameters[2] = new SqlParameter("@modifiedDate", DateTime.Now);
            sqlParameters[3] = new SqlParameter("@fromDate", fromTime);
            sqlParameters[4] = new SqlParameter("@toDate", toTime);

            DataTable dt = SqlHelper.ExecuteDataTable(SqlHelper.QCConnectionString, CommandType.StoredProcedure, storePro, sqlParameters);
            return dt;
        }


        #region  Get and Update and Delete task TAT by location




        [HttpGet]
        public HttpResponseMessage GetTaskTATByLocation(string location_str)
        {
            DateTime fromDate = DateTime.Now.AddDays(-10);
            DateTime toDate = DateTime.Now.AddDays(1);
            string result = "";

            string sqlText = "select taskid, CONVERT(varchar(100), TAT, 20) as TAT from [dbo].[TaskTAT] t where t.ModifiedDate between  '" + fromDate + "'  and  '" + toDate + "' and [Location]='" + location_str + "'  order by t.TaskID desc";
            DataTable dt = SqlHelper.ExecuteDataTable(SqlHelper.QCConnectionString, CommandType.Text, sqlText);
            if (dt != null && dt.Rows.Count > 0)
            {
                string tt = "";
                for (int i = 0; i < dt.Rows.Count; i++)
                {
                    string TaskID = dt.Rows[i][0].ToString();
                    string TATstr = dt.Rows[i][1].ToString();
                    tt = tt + TaskID + "&" + TATstr + "|";
                }
                if (tt != "")
                    tt = tt.Substring(0, tt.Length - 1);
                result = tt;


                var response = Request.CreateResponse(HttpStatusCode.OK);

                response.Content = new StringContent(result, Encoding.UTF8);
                response.Headers.CacheControl = new CacheControlHeaderValue()
                {
                    MaxAge = TimeSpan.FromMinutes(20)
                };

                return response;
            }
            else
            {
                var response = Request.CreateResponse(HttpStatusCode.OK);

                response.Content = new StringContent(result, Encoding.UTF8);
                response.Headers.CacheControl = new CacheControlHeaderValue()
                {
                    MaxAge = TimeSpan.FromMinutes(20)
                };

                return response;
            }
        }

        [HttpGet]
        public HttpResponseMessage GetTaskTATByTaskIDAndLocation(string argTaskId, string location_str)
        {
            string sqlText = "select * from [dbo].[TaskTAT] t where  t.TaskID = " + argTaskId + "  and [location]='" + location_str + "'";
            DataTable dt = SqlHelper.ExecuteDataTable(SqlHelper.QCConnectionString, CommandType.Text, sqlText);
            if (dt != null && dt.Rows.Count > 0)
            {
                string TATstr = dt.Rows[0][1].ToString();

                string result = TATstr;
                var response = Request.CreateResponse(HttpStatusCode.OK);

                response.Content = new StringContent(result, Encoding.UTF8);
                response.Headers.CacheControl = new CacheControlHeaderValue()
                {
                    MaxAge = TimeSpan.FromMinutes(20)
                };

                return response;
            }
            else
            {
                var response = Request.CreateResponse(HttpStatusCode.NoContent);
                return response;
            }
        }

        [HttpGet]
        public HttpResponseMessage ChangeTaskTATByLocation(string argTaskId, DateTime argTat, string location_str)
        {
            string result = "";

            DataTable dt = ChangesTaskTATByLocation(argTaskId, argTat, location_str);

            if (dt.Rows.Count > 0)
            {
                result = "Modified successfully!";
                var response = Request.CreateResponse(HttpStatusCode.OK);

                response.Content = new StringContent(result, Encoding.UTF8);
                response.Headers.CacheControl = new CacheControlHeaderValue()
                {
                    MaxAge = TimeSpan.FromMinutes(20)
                };

                return response;
            }
            else
            {
                var response = Request.CreateResponse(HttpStatusCode.NoContent);
                return response;
            }


        }

        [HttpGet]
        public HttpResponseMessage DeleteTaskTATByTaskIdAndLocation(string argTaskId, string location_str)
        {
            try
            {

                string sqlText = "DELETE [dbo].[TaskTAT] where [TaskID]='" + argTaskId + "' and [Location]='" + location_str + "'";
                object obj = SqlHelper.ExecuteScalar(SqlHelper.QCConnectionString, CommandType.Text, sqlText);

                if (obj == null)
                {
                    string result = "Modified successfully!";
                    var response = Request.CreateResponse(HttpStatusCode.OK);

                    response.Content = new StringContent(result, Encoding.UTF8);
                    response.Headers.CacheControl = new CacheControlHeaderValue()
                    {
                        MaxAge = TimeSpan.FromMinutes(20)
                    };

                    return response;
                }
                else
                {
                    var response = Request.CreateResponse(HttpStatusCode.NoContent);
                    return response;
                }

            }
            catch (Exception ex)
            {
                var response = Request.CreateResponse(HttpStatusCode.NotModified);
                return response;
            }

        }



        public DataTable ChangesTaskTATByLocation(string argTaskId, DateTime argTat, string location_str)
        {

            DateTime fromTime = DateTime.Now.AddDays(-10);
            DateTime toTime = DateTime.Now.AddDays(1);
            string storePro = "ChangeTaskTATByTaskIdandLocation";

            SqlParameter[] sqlParameters = new SqlParameter[6];

            sqlParameters[0] = new SqlParameter("@taskId", argTaskId);
            sqlParameters[1] = new SqlParameter("@taskTAT", argTat);
            sqlParameters[2] = new SqlParameter("@modifiedDate", DateTime.Now);
            sqlParameters[3] = new SqlParameter("@fromDate", fromTime);
            sqlParameters[4] = new SqlParameter("@toDate", toTime);
            sqlParameters[5] = new SqlParameter("@location", location_str);

            DataTable dt = SqlHelper.ExecuteDataTable(SqlHelper.QCConnectionString, CommandType.StoredProcedure, storePro, sqlParameters);
            return dt;
        }

        #endregion

        [HttpGet]
        public HttpResponseMessage LogInForTrafficControl(string argUserName, string argPassword)
        {
            RestClient.init();
            TCCLoginDTO tccLoginDTO = RestClient.Login(argUserName, argPassword);


            if (tccLoginDTO.StatusCode == HttpStatusCode.OK)
            {
                string result = "OK";
                var response = Request.CreateResponse(HttpStatusCode.OK);

                response.Content = new StringContent(result, Encoding.UTF8);
                response.Headers.CacheControl = new CacheControlHeaderValue()
                {
                    MaxAge = TimeSpan.FromMinutes(20)
                };

                return response;
            }
            else
            {
                var response = Request.CreateResponse(HttpStatusCode.Unauthorized);
                return response;
            }

        }

        [HttpGet]
        public HttpResponseMessage InsertDownloadFileRecordsControl(string argUserId, string argUserName, string argTeamName, string argTaskId, string argFileType, string argFileName)
        {
            string result;
            SqlParameter[] sqlParameters = new SqlParameter[7];
            sqlParameters[0] = new SqlParameter("@userId", Convert.ToInt32(argUserId));
            sqlParameters[1] = new SqlParameter("@username", argUserName);
            sqlParameters[2] = new SqlParameter("@teamName", argTeamName);
            sqlParameters[3] = new SqlParameter("@taskId", argTaskId);
            sqlParameters[4] = new SqlParameter("@fileName", argFileName);
            sqlParameters[5] = new SqlParameter("@fileType", argFileType);
            sqlParameters[6] = new SqlParameter("@downloadDT", DateTime.Now);

            string storePro = "InsertDownloadFileRecords";
            try
            {
                DataTable dt = SqlHelper.ExecuteDataTable(SqlHelper.QCConnectionString, CommandType.StoredProcedure, storePro, sqlParameters);

                if (dt.Rows.Count > 0)
                {
                    result = "insert successfully!";
                    var response = Request.CreateResponse(HttpStatusCode.OK);

                    response.Content = new StringContent(result, Encoding.UTF8);
                    response.Headers.CacheControl = new CacheControlHeaderValue()
                    {
                        MaxAge = TimeSpan.FromMinutes(20)
                    };

                    return response;
                }
                else
                {
                    var response = Request.CreateResponse(HttpStatusCode.NoContent);
                    return response;
                }
            }
            catch (Exception ex)
            {
                var response = Request.CreateResponse(HttpStatusCode.NotModified);
                return response;
            }
        }

        [HttpGet]
        public HttpResponseMessage InsertOrUpdateOpsTaskShiftDutyUserInfo(string argUserId, string argUserName,string argShortName, string argShiftDuty, string argTeamId,string argStatus)
        {
            string result;
            SqlParameter[] sqlParameters = new SqlParameter[6];
            sqlParameters[0] = new SqlParameter("@userId", Convert.ToInt32(argUserId));
            sqlParameters[1] = new SqlParameter("@username", argUserName);
            sqlParameters[2] = new SqlParameter("@shortName", argShortName);
            sqlParameters[3] = new SqlParameter("@TeamId", Convert.ToInt32(argTeamId));
            sqlParameters[4] = new SqlParameter("@ShiftDuty", Convert.ToInt32(argShiftDuty));
            sqlParameters[5] = new SqlParameter("@Status", Convert.ToInt32(argStatus));

            string storePro = "InsertOpsShiftDutyUserInfo";
            try
            {
                DataTable dt = SqlHelper.ExecuteDataTable(SqlHelper.QCConnectionString, CommandType.StoredProcedure, storePro, sqlParameters);

                if (dt.Rows.Count > 0)
                {
                    result = "insert successfully!";
                    var response = Request.CreateResponse(HttpStatusCode.OK);

                    response.Content = new StringContent(result, Encoding.UTF8);
                    response.Headers.CacheControl = new CacheControlHeaderValue()
                    {
                        MaxAge = TimeSpan.FromMinutes(20)
                    };

                    return response;
                }
                else
                {
                    var response = Request.CreateResponse(HttpStatusCode.NoContent);
                    return response;
                }
            }
            catch (Exception ex)
            {
                var response = Request.CreateResponse(HttpStatusCode.NotModified);
                return response;
            }

        }

        [HttpGet]
        public HttpResponseMessage GetOpsShiftDutyUserInfoByTeamIdOrUserId(string argTeamId,string argUserId)
        {
            try
            {
                SqlParameter[] sqlParameters = new SqlParameter[2];
                sqlParameters[0] = new SqlParameter("@userId", Convert.ToInt32(argUserId));
                sqlParameters[1] = new SqlParameter("@TeamId", Convert.ToInt32(argTeamId));
                string storePro = "GetOpsShiftDutyUserInfoByUserIdOrTeamId";
                DataSet ds = SqlHelper.ExecuteDataset(SqlHelper.QCConnectionString, CommandType.StoredProcedure, storePro, sqlParameters);

                if (ds != null)
                {
                    if (ds.Tables.Count > 0)
                    {
                        if (ds.Tables[0].Rows.Count > 0)
                        {
                            string result = GenTxt(ds);
                            var response = Request.CreateResponse(HttpStatusCode.OK);

                            response.Content = new StringContent(result, Encoding.UTF8);
                            response.Headers.CacheControl = new CacheControlHeaderValue()
                            {
                                MaxAge = TimeSpan.FromMinutes(20)
                            };
                            return response;

                        }
                        else
                        {
                            var response = Request.CreateResponse(HttpStatusCode.NoContent);
                            return response;
                        }

                    }
                    else
                    {
                        var response = Request.CreateResponse(HttpStatusCode.NoContent);
                        return response;
                    }
                }
                else
                {
                    var response = Request.CreateResponse(HttpStatusCode.NoContent);
                    return response;
                }

            }
            catch (Exception ex)
            {
                var response = Request.CreateResponse(HttpStatusCode.NotModified);
                return response;
            }

        }

       


    }
}