﻿using System;
using System.Net.Http;
using System.Threading.Tasks;
using System.Net;
using System.Configuration;
using System.Diagnostics;
using HKTrafficPageForWeb;

namespace HKTrafficPageForWeb
{
    //Implentment IDispose?
    //public class ResourceServerRestClient : IResourceServerRestClient 
    public static class RestClient  //: IRestClient
    {
        private static TokenProvider _tokenProvider;
        private static JsonManager _jsonManager;





        public static void init()
        {
            _tokenProvider = new TokenProvider();
            _jsonManager = new JsonManager();
            ConfigurateHttpClient(Client, null);
        }

        // you can inject the interfaces
        //public RestClient()
        // {
        //add excpetion manger

        //_tokenProvider = new TokenProvider();
        // _jsonManager = new JsonManager();
        //ConfigurateHttpClient(Client, null);
        // }

        // this is just to demonstrate a simple reuse technique. you can do it in other ways. (singleton, DI, static)
        private static HttpClient _client;
         public static HttpClient Client => _client ?? (_client = new HttpClient());

        public static async Task<ResponseDTO<T>> GetAsync<T>(string uri)
        {
            return await InvokeAsync<ResponseDTO<T>>(
               client => client.GetAsync(uri),
               //response => response.Content.ReadAsAsync<T>());
               response => LoadToDTO<T>(response.Content));
        }

        //Post and ingore the reponse
        public static Task PostAsJsonAsync(object data, string uri)
        {
            return InvokeAsync<object>(
                client => client.PostAsJsonAsync(uri, data));
        }

        //Post and return the response
        public static Task<ResponseDTO<T>> PostAsJsonAsync<T>(object data, string uri)
        {
            return InvokeAsync<ResponseDTO<T>>(
               client => client.PostAsJsonAsync(uri, data),
               //response => response.Content.ReadAsAsync<T>());
               response => LoadToDTO<T>(response.Content)); //callback
        }

        //Put  and ingore the response
        public static Task PutAsJsonAsync(object data, string uri)
        {
            return InvokeAsync<object>(
                client => client.PutAsJsonAsync(uri, data));
        }

        //Put  and return the reponse
        public static Task<ResponseDTO<T>> PutAsJsonAsync<T>(object data, string uri)
        {
            return InvokeAsync<ResponseDTO<T>>(
                client => client.PutAsJsonAsync(uri, data),
               //response => response.Content.ReadAsAsync<T>());
               response => LoadToDTO<T>(response.Content)); //callback
        }


        //Put  and ingore the response
        public static Task DeleteAsync(string uri)
        {
            return InvokeAsync<object>(
                client => client.DeleteAsync(uri));
        }

        //Put  and return the reponse
        public static Task<ResponseDTO<T>> DeleteAsync<T>(string uri)
        {
            return InvokeAsync<ResponseDTO<T>>(
                client => client.DeleteAsync(uri),
               //response => response.Content.ReadAsAsync<T>());
               response => LoadToDTO<T>(response.Content)); //callback
        }



        //will only reutrn token/ error message
        public static TCCLoginDTO Login(string username, string pw) 
        {
            return  Task.Run(()=>_tokenProvider.GetTokenAsync(username, pw)).Result;
        }

        public static bool CheckIsExiteUserName(string username)
        {
            return Task.Run(() => _tokenProvider.CheckIsExiteUserName(username)).Result;
           
        }


        private static async Task<ResponseDTO<T>> LoadToDTO<T>(HttpContent content)
        {
            try
            {
                var result = await content.ReadAsStringAsync();
                JsonManager jsonm = new JsonManager();
                return jsonm.ParseJsonString<T>(result);
                
            }
            catch (Exception ex)
            {
                throw;
            }

        }
 
             
        private static async Task<T> InvokeAsync<T>(
            Func<HttpClient, Task<HttpResponseMessage>> operation,
            Func<HttpResponseMessage, Task<T>> actionOnResponse = null)
        {
            if (operation == null)
                throw new ArgumentNullException(nameof(operation));
           HttpResponseMessage response = await operation(Client).ConfigureAwait(false);

            //add log

            //token expired, redo one
            if (response.StatusCode == HttpStatusCode.Unauthorized)
            {
                //need to re-do and renew the token
                //renewTokenForHttpClient();
                response = await operation(Client).ConfigureAwait(false);
            }


            if (!response.IsSuccessStatusCode)
            {
                var exception = new Exception($"Resource server returned an error. StatusCode : {response.StatusCode}");
                exception.Data.Add("StatusCode", response.StatusCode);
                //Should return error result.
                throw exception;

            }

            if (actionOnResponse != null)
            {
                var result =  await actionOnResponse(response).ConfigureAwait(false);
                Debug.WriteLine("return result from async callback " + result.ToString());
                return result;
            }
            else
            {
                return default(T);
            }
        }
        
        
        //Get Token from TCC
        //Not in use, change to use static token from web.config
        private static string GetToken()
        {
            /*   // if IsTokenNullOrExpired return null and not string.Empty, you can do the foloowing:
               var token = await _tokenProvider.IsTokenNullOrExpired() ?? await _tokenProvider.GetTokenAsync();
               if (string.IsNullOrEmpty(token))
               {
                   var exception = new Exception();
                   exception.Data.Add("StatusCode", HttpStatusCode.Unauthorized);
                   throw exception;
               }
               //else, do this:
               string token = await _tokenProvider.IsTokenNullOrExpired();(*/
            


            string token = _tokenProvider.GetTokenAsync().Result;
            if (string.IsNullOrEmpty(token))
            {
                //retry
                token = _tokenProvider.GetTokenAsync().Result;
                if (string.IsNullOrEmpty(token))
                {
                    var exception = new Exception();
                    exception.Data.Add("StatusCode", HttpStatusCode.Unauthorized);
                    throw exception;
                }
            }
            return token;
        }

        //Get Token from web.config
        private static string GetTokenFromConfig()
        {
            string token = ConfigurationManager.AppSettings["ServiceToken"];

            if (string.IsNullOrEmpty(token))
            {
                token = _tokenProvider.GetTokenAsync().Result;
                if (string.IsNullOrEmpty(token))
                {
                    var exception = new ConfigurationErrorsException("Token not found");
                    throw exception;
                }
            }
            return token;
        }


        private static void renewTokenForHttpClient()
        {
            Client.DefaultRequestHeaders.Remove("Token");
            Client.DefaultRequestHeaders.Add("Token", GetToken());
        }
        
        private static void ConfigurateHttpClient(HttpClient client, string resourceServiceClientName)
        {
            // do this first
            string baseAddress = ConfigurationManager.AppSettings["TCCApiBaseUrl"];

            if (String.IsNullOrEmpty(baseAddress))
            {
                // Key not found 
                throw new ConfigurationErrorsException("Cannot find Api Base URL.");
            }

            client.BaseAddress = new Uri(baseAddress);           
            client.Timeout = new TimeSpan(0, 0, 0, 30);
            client.DefaultRequestHeaders.Accept.Clear();
            
            
            //Add token
            //Get token from web.config
            //client.DefaultRequestHeaders.Add("Token", GetToken());
            client.DefaultRequestHeaders.Add("Token", GetTokenFromConfig());

            //Define as Json body
            //client.DefaultRequestHeaders.Add("Content-type","application/json");
            //client.DefaultRequestHeaders.Authorization = new AuthenticationHeaderValue("Bearer", bearerToken);
        }



    }
}