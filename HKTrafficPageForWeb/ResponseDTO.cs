﻿using System.Collections.Generic;
using System.Runtime.Serialization;
using HKTrafficPageForWeb;
namespace HKTrafficPageForWeb
{
    [DataContract]
    public class ResponseDTO<T>
    {
        //Generic Data
        [DataMember]
        public IList<T> Data { get; set; }
        [DataMember]
        public int ErrorCode { get; set; }
        [DataMember]
        public string Message { get; set; }
        [DataMember]
        public int ResultType { get; set; }
        [DataMember]
        public object StackTrace { get; set; }
        [DataMember]
        public object ValidationError { get; set; }
        [DataMember]
        public int FailureType { get; set; }
    }

    public class ValidationErrorItem
    {
        public string Code { get; set; }
        public string Message { get; set; }
        public string Field { get; set; }
    }



}