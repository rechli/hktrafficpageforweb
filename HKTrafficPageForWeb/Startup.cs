﻿using Microsoft.Owin;
using Owin;

[assembly: OwinStartupAttribute(typeof(HKTrafficPageForWeb.Startup))]
namespace HKTrafficPageForWeb
{
    public partial class Startup {
        public void Configuration(IAppBuilder app) {
            ConfigureAuth(app);
        }
    }
}
