ALTER PROCEDURE [dbo].[ChangeTaskTATByTaskId]
@taskId nvarchar(50),
@taskTAT datetime,
@modifiedDate datetime,
@fromDate	datetime,
@toDate		datetime

AS
BEGIN
	if exists(select * from [dbo].[TaskTAT] t where t.taskid=@taskId )
	begin
		update [dbo].[TaskTAT] set [TAT]=@taskTAT,[ModifiedDate]=@modifiedDate where taskid=@taskId
	end
	else
	begin
		INSERT INTO [dbo].[TaskTAT]([TaskID], [TAT], [ModifiedDate]) values(@taskId,@taskTAT,@modifiedDate);
	end

	select * from [dbo].[TaskTAT] t where t.ModifiedDate between @fromDate and @toDate
	
END