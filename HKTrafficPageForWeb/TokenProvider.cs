﻿using System;
using System.Threading.Tasks;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Text;
using System.Collections.Generic;
using System.Linq;
using System.Configuration;
using HKTrafficPageForWeb;
using System.Net;

namespace HKTrafficPageForWeb
{
    public class TokenProvider
    {
        private HttpClient _client;
        public HttpClient Client => _client ?? (_client = new HttpClient());

        public TokenProvider()
        {
            // do this first
            string baseAddress = ConfigurationManager.AppSettings["TCCApiBaseUrl"];

            if (String.IsNullOrEmpty(baseAddress))
            {
                // Key not found 
                throw new ArgumentNullException("Cannot find Api Base URL.");
            }

            Uri baseUri = new Uri(baseAddress);
            Client.BaseAddress = baseUri;
        }

        internal async Task<string> GetTokenAsync()
        {

            ServicePointManager.Expect100Continue = true;
            ServicePointManager.SecurityProtocol = SecurityProtocolType.Tls12 | SecurityProtocolType.Tls11 | SecurityProtocolType.Tls;
            ServicePointManager.ServerCertificateValidationCallback = (sender, certificate, chain, errors) => true;
            var credentials = Convert.ToBase64String(Encoding.ASCII.GetBytes(string.Format("{0}:{1}", "rmsapp", "toppanvite@12345")));
            Client.DefaultRequestHeaders.Authorization = new AuthenticationHeaderValue("Basic", credentials);


            using (HttpResponseMessage response = await _client.PostAsync("account/login", null))
                using (HttpContent content = response.Content)
                {
                    HttpHeaders headers  = response.Headers;

                    IEnumerable<string> values;
                    if (headers.TryGetValues("Token", out values))
                    {
                        return values.First();
                    }

                    // ... Read the string.
                    string result = await content.ReadAsStringAsync();

                    // ... Display the result.
                    if (result != null &&
                        result.Length >= 50)
                    {
                        Console.WriteLine(result.Substring(0, 50) + "...");
                    }
                    return result;
                }
          
        }


        internal async Task<TCCLoginDTO> GetTokenAsync(string user ,string pw)
        {
            ServicePointManager.Expect100Continue = true;
            ServicePointManager.SecurityProtocol = SecurityProtocolType.Tls12 | SecurityProtocolType.Tls11 | SecurityProtocolType.Tls;
            ServicePointManager.ServerCertificateValidationCallback = (sender, certificate, chain, errors) => true;
            var credentials = Convert.ToBase64String(Encoding.ASCII.GetBytes(string.Format("{0}:{1}", user, pw)));
            Client.DefaultRequestHeaders.Authorization = new AuthenticationHeaderValue("Basic", credentials);

            using (HttpResponseMessage response = await _client.PostAsync("account/login", null))
                using (HttpContent content = response.Content)
                {
                    HttpHeaders headers = response.Headers;
                    IEnumerable<string> values;
                    string token="";
                    if (headers.TryGetValues("Token", out values))
                    {
                        token = values.First();    
                    }
                    
                    // ... Read the string.
                    string result = await content.ReadAsStringAsync();
                    // ... Display the result.
                    if (result != null &&
                        result.Length >= 50)
                    {
                        Console.WriteLine(result.Substring(0, 50) + "...");
                    }

                    return new TCCLoginDTO { Token = token, Message = result, StatusCode = response.StatusCode };
                }
         
        }


        internal async Task<bool> CheckIsExiteUserName(string user)
        {

          //  var credentials = Convert.ToBase64String(Encoding.ASCII.GetBytes(string.Format("{0}", user)));
           // Client.DefaultRequestHeaders.Authorization = new AuthenticationHeaderValue("Basic", credentials);

            using (HttpResponseMessage response = await _client.PostAsync("users/GetAllUsers", null))
            using (HttpContent content = response.Content)
            {
                HttpHeaders headers = response.Headers;
                IEnumerable<string> values;
                string token = "";
                if (headers.TryGetValues("Token", out values))
                {
                    token = values.First();
                }

                // ... Read the string.
                string result = await content.ReadAsStringAsync();
                // ... Display the result.
                if (result != null &&
                    result.Length >= 50)
                {
                    Console.WriteLine(result.Substring(0, 50) + "...");
                }
                new TCCLoginDTO { Token = token, Message = result, StatusCode = response.StatusCode };
                return true;
            }

        }
    }
}