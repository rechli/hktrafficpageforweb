﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Net.Http;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;


namespace HKTrafficPageForWeb
{
    public partial class HKTrafficPage : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {


            HttpCookie httpCookie = Request.Cookies.Get("userName");


            if (httpCookie != null && httpCookie.Value.Length > 0)
            {
                //TCCLoginDTO tccLoginDTO = RestClient.Login("Chrisleung", "Tcc2020!");
                //Response.Write("<script>alet('" + tccLoginDTO.Token + "----" + tccLoginDTO.Message + "----" + tccLoginDTO.StatusCode +
                //    "');</script>");
                //if (!IsPostBack)
                //{
                    GetData(null, null);
                //}
            }
            else
            {
                IdentityHelper.RedirectToReturnUrl("~/Account/Login.aspx", Response);
            }
        }

        private void GetData(string argJobID,string argJobName)
        {

            DateTime fromTime = DateTime.Now.AddDays(-7);
            DateTime toTime = DateTime.Now.AddDays(1);

            string connectString = "Data Source=tccsecondarydbserver.database.windows.net,1433;Initial Catalog=TCC_PROD_DB;User Id=tpvadmin;Password=@@12345toppan!!";
            string storePro = "GetOutStandingTaskInfoForHKTraffic";

            SqlParameter[] sqlParameters = new SqlParameter[6];



            if (argJobName != null)
            { sqlParameters[0] = new SqlParameter("@jobName", argJobName); }
            else
            { sqlParameters[0] = new SqlParameter("@jobName", string.Empty); }


            sqlParameters[1] = new SqlParameter("@isShowPm", 0);
            sqlParameters[2] = new SqlParameter("@dateFrom", fromTime);
            sqlParameters[3] = new SqlParameter("@dateTo", toTime);

            if (argJobID != null)
            { sqlParameters[4] = new SqlParameter("@jobId", argJobID); }
            else
            { sqlParameters[4] = new SqlParameter("@jobId", string.Empty); }

            sqlParameters[5] = new SqlParameter("@isDoneBuctet", 0);

            DataSet ds = SqlHelper.ExecuteDataset(connectString, CommandType.StoredProcedure, storePro, sqlParameters);
            if (ds.Tables.Count > 0)
            {
                this.GridView1.DataSource = ds.Tables[0];
                this.GridView1.DataBind();

                CountData(ds);

                ViewState["TableData"] = ds.Tables[0];
            }
        }
        private void CountData(DataSet ds)
        {
            List<LocationPages> list = new List<LocationPages>();
            LocationPages conversionPages = new LocationPages();
            conversionPages.SubTaskType = "Conversion";

            LocationPages aaPages = new LocationPages();
            aaPages.SubTaskType = "AA";

            LocationPages hkexPages = new LocationPages();
            hkexPages.SubTaskType = "HKEX";

            LocationPages otherPages = new LocationPages();
            otherPages.SubTaskType = "Others";

            DataTable dt = ds.Tables[0];
            for (int i = 0; i < dt.Rows.Count; i++)
            {
                DataRow dr = dt.Rows[i];
                string subTaskType = dr["SubTaskType"].ToString();
                string team = dr["TeamName"].ToString();
                int currentPages = Convert.ToInt32(dr["TotalNoOfPages"].ToString());

                if (team.Contains("HK"))
                {
                    if (subTaskType == "Conversion")
                    {
                        conversionPages.HKPages += currentPages;
                    }
                    else if (subTaskType == "AA'S" || subTaskType == "Global Search")
                    {
                        aaPages.HKPages += currentPages;
                    }
                    else if (subTaskType.Contains("HKEX"))
                    {
                        hkexPages.HKPages += currentPages;
                    }
                    else
                        otherPages.HKPages += currentPages;
                }
                if (team.Contains("SZ"))
                {
                    if (subTaskType == "Conversion")
                    {
                        conversionPages.SZPages += currentPages;
                    }
                    else if (subTaskType == "AA'S" || subTaskType == "Global Search")
                    {
                        aaPages.SZPages += currentPages;
                    }
                    else if (subTaskType.Contains("HKEX"))
                    {
                        hkexPages.SZPages += currentPages;
                    }
                    else
                        otherPages.SZPages += currentPages;
                }
                if (team.Contains("MNL"))
                {
                    if (subTaskType == "Conversion")
                    {
                        conversionPages.MNLPages += currentPages;
                    }
                    else if (subTaskType == "AA'S" || subTaskType == "Global Search")
                    {
                        aaPages.MNLPages += currentPages;
                    }
                    else if (subTaskType.Contains("HKEX"))
                    {
                        hkexPages.MNLPages += currentPages;
                    }
                    else
                        otherPages.MNLPages += currentPages;
                }
                if (team.Contains("SG"))
                {
                    if (subTaskType == "Conversion")
                    {
                        conversionPages.SGPages += currentPages;
                    }
                    else if (subTaskType == "AA'S" || subTaskType == "Global Search")
                    {
                        aaPages.SGPages += currentPages;
                    }
                    else if (subTaskType.Contains("HKEX"))
                    {
                        hkexPages.SGPages += currentPages;
                    }
                    else
                        otherPages.SGPages += currentPages;
                }
                if (team.Contains("TW"))
                {
                    if (subTaskType == "Conversion")
                    {
                        conversionPages.TWPages += currentPages;
                    }
                    else if (subTaskType == "AA'S" || subTaskType == "Global Search")
                    {
                        aaPages.TWPages += currentPages;
                    }
                    else if (subTaskType.Contains("HKEX"))
                    {
                        hkexPages.TWPages += currentPages;
                    }
                    else
                        otherPages.TWPages += currentPages;
                }
                if (team.Contains("BJ"))
                {
                    if (subTaskType == "Conversion")
                    {
                        conversionPages.BJPages += currentPages;
                    }
                    else if (subTaskType == "AA'S" || subTaskType == "Global Search")
                    {
                        aaPages.BJPages += currentPages;
                    }
                    else if (subTaskType.Contains("HKEX"))
                    {
                        hkexPages.BJPages += currentPages;
                    }
                    else
                        otherPages.BJPages += currentPages;
                }


            }
            list.Add(conversionPages);
            list.Add(aaPages);
            list.Add(hkexPages);
            list.Add(otherPages);

            this.GV_ForCountNumber.DataSource = list;
            this.GV_ForCountNumber.DataBind();
        }

        protected void GridView1_DataBound(object sender, EventArgs e)
        {
            for (int i = 0; i < this.GridView1.Rows.Count; i++)
            {
                string taskTAT = this.GridView1.Rows[i].Cells[7].Text.ToString();

                Label taskIndicatorLabel = this.GridView1.Rows[i].FindControl("Label_TaskIn") as Label;
                Label taskRevisionLabel = this.GridView1.Rows[i].FindControl("Label_TaskRevsion") as Label;

                string taskIndicator = taskIndicatorLabel.Text;
                string taskRevision = taskRevisionLabel.Text;

                if (taskTAT == "Filling today")
                {
                    this.GridView1.Rows[i].BackColor = System.Drawing.Color.Red;
                }
                if (taskTAT == "Rush Remote")
                {
                    this.GridView1.Rows[i].BackColor = System.Drawing.Color.Yellow;
                }


                if (taskIndicator.Contains("3"))
                {
                    Image U = this.GridView1.Rows[i].FindControl("Image_U") as Image;
                    U.Visible = true;
                }
                if (taskIndicator.Contains("4"))
                {
                    Image L = this.GridView1.Rows[i].FindControl("Image_L") as Image;
                    L.Visible = true;
                }
                if (taskIndicator.Contains("7"))
                {
                    Image UL = this.GridView1.Rows[i].FindControl("Image_UL") as Image;
                    UL.Visible = true;
                }
                if (taskRevision.Contains("1"))
                {
                    Image R = this.GridView1.Rows[i].FindControl("Image_R") as Image;
                    R.Visible = true;
                }
            }
        }

        protected void GridView1_Sorting(object sender, GridViewSortEventArgs e)
        {
            if (ViewState["SortDirection"] == null)
                ViewState["SortDirection"] = "DESC";
            if (ViewState["SortDirection"].ToString() == "ASC")
                ViewState["SortDirection"] = "DESC";
            else
                ViewState["SortDirection"] = "ASC";
            ViewState["SortExpression"] = e.SortExpression;

            if (ViewState["SortDirection"] != null)
            {
                DataTable ds = ViewState["TableData"] as DataTable;
                if (ViewState["SortDirection"] == null)
                {
                    GridView1.DataSource = ds.DefaultView;
                }
                else
                {
                    DataView SortedDV = new DataView(ds);
                    SortedDV.Sort = ViewState["SortExpression"].ToString() + " " +
                    ViewState["SortDirection"].ToString();
                    GridView1.DataSource = SortedDV;
                }
                this.GridView1.DataBind();
            }
        }

        //protected void ButtonSearch_Click(object sender, EventArgs e)
        //{
        //    GetData(this.TextBoxJobID.Text.Trim(), this.TextBoxJobName.Text.Trim());
        //}
    }

    public class LocationPages
    {
        public LocationPages()
        {

        }

        private string subTaskType;
        public string SubTaskType
        {
            get
            {
                return subTaskType;
            }

            set
            {
                subTaskType = value;
            }
        }

       

       
        public int HKPages
        {
            get
            {
                return hkPages;
            }

            set
            {
                hkPages = value;
            }
        }

        public int MNLPages
        {
            get
            {
                return mnlPages;
            }

            set
            {
                mnlPages = value;
            }
        }

        public int SZPages
        {
            get
            {
                return szPages;
            }

            set
            {
                szPages = value;
            }
        }

        public int BJPages
        {
            get
            {
                return bjPages;
            }

            set
            {
                bjPages = value;
            }
        }

        public int TWPages
        {
            get
            {
                return twPages;
            }

            set
            {
                twPages = value;
            }
        }

        public int SGPages
        {
            get
            {
                return sgPages;
            }

            set
            {
                sgPages = value;
            }
        }

        private int hkPages = 0;

        private int mnlPages = 0;

        private int szPages = 0;

        private int bjPages = 0;

        private int twPages = 0;

        private int sgPages = 0;
    }
}