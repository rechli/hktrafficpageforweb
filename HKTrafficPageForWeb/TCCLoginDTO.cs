﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Runtime.Serialization;
using System.Net;
using HKTrafficPageForWeb;

namespace HKTrafficPageForWeb
{
    [DataContract]
    public class TCCLoginDTO
    {
        [DataMember]
        public string Token { get; set; }
        [DataMember]
        public string Message { get; set; }
        [DataMember]
        public HttpStatusCode  StatusCode{ get; set;  }
    }
}